#-------------------------------------------------
#
# Project created by QtCreator 2015-01-18T19:03:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quicknote
TEMPLATE = app
INCLUDEPATH += code code/model code/controls
CONFIG+= c++11

SOURCES += code/main.cpp\
    code/quicknote.cpp \
    code/utils.cpp \
    code/model/note.cpp \
    code/model/notes.cpp \
    code/model/notesnavigator.cpp \
    code/controls/texteditbox.cpp \
    code/controls/noteslistwidget.cpp \

HEADERS  += code/quicknote.hpp \
    code/strutils.hpp \
    code/utils.hpp \
    code/model/note.hpp \
    code/model/notes.hpp \
    code/model/notesnavigator.hpp \
    code/controls/texteditbox.hpp \
    code/controls/noteslistwidget.hpp \

FORMS    += code/quicknote.ui

DISTFILES += \
    run.cmd \
    tests/runtest.cmd



