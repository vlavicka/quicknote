function! ChangeDirectory(dirpath)
    let old_dir = getcwd()
    execute "cd ".a:dirpath
    return old_dir
endfunction

function! GitFindRepositoryRoot()
    let l:gitrepo = finddir('.git', '.;')
    if l:gitrepo == ".git"
        let l:gitrepo = "."
    else
        let l:gitrepo = substitute(l:gitrepo, "\/.git", "", "")
    endif
    return l:gitrepo
endfunction

function! QuickNoteBuild()
    let cur_dir = ChangeDirectory(GitFindRepositoryRoot())
    exec '!./build.sh'
    call ChangeDirectory(cur_dir)
endfunction

function! QuickNoteRegression()
    let l:cur_dir = ChangeDirectory(GitFindRepositoryRoot()."/tests/py")
    exec '!./runall.sh'
    call ChangeDirectory(l:cur_dir)
endfunction

" key shortcuts definition
map <F7> :w<CR>:call QuickNoteBuild()<CR>
map <F6> :w<CR>:call QuickNoteRegression()<CR>
