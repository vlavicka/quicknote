#include <ctime>
#include <sstream>

#include "../utils.hpp"
#include "note.hpp"

namespace QuickNotes {

Note::Note() 
    : ndate(encode_time(std::time(nullptr)))
    , ntext(std::string()) { }

Note::Note(const std::string &t) 
    : ndate(encode_time(std::time(nullptr)))
    , ntext(t) {}

Note::Note(const std::string &d, const std::string &t)
    : ndate(d), ntext(t) {}

int Note::days() {
    auto diff = std::difftime(std::time(nullptr), QuickNotes::decode_time(ndate));
    return int(diff / 3600 / 24);
}

std::string Note::date() {
    return ndate;
}

void Note::set_date(std::string &d) {
    ndate = d;
}

std::string Note::text() {
    return ntext;
}

void Note::set_text(std::string &t) {
    ntext = t;
}

std::string Note::to_string() {
    std::ostringstream strs;
    strs << "Note(date=" << ndate 
         << ", text=" << ntext << ")";;
    return strs.str();
}

};
