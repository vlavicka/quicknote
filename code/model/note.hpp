#ifndef HEADER_QUICKNOTE_MODEL_NOTE
#define HEADER_QUICKNOTE_MODEL_NOTE

#include <string>

namespace QuickNotes {

class Note {
private:
    std::string ndate;
    std::string ntext;
public:
    Note();
    Note(const std::string &t);
    Note(const std::string &d, const std::string &t);

    std::string date();
    void set_date(std::string &d);
    std::string text();
    void set_text(std::string &t);

    int days();
    std::string to_string();
};

} // namespace QuickNote

#endif
