#ifndef HEADER_QUICKNOTE_MODEL_NOTESNAVIGATOR
#define HEADER_QUICKNOTE_MODEL_NOTESNAVIGATOR

#include <string>

namespace QuickNotes {

class Notes;

class NotesNavigator {
private:
    int cursor;
    Notes * notes;
public:
    NotesNavigator();
    NotesNavigator(Notes * n);

    void next();
    void previous();

    void reset();
    void reset_end();
    int current();
    int size();
    bool can_append();
    bool can_update();

    std::string to_string();
};

} // namespace QuickNote

#endif
