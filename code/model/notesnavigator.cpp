#include <iostream>
#include <sstream>

#include "strutils.hpp"
#include "model/note.hpp"
#include "model/notes.hpp"
#include "model/notesnavigator.hpp"

namespace QuickNotes {

NotesNavigator::NotesNavigator() : cursor(0) {}
NotesNavigator::NotesNavigator(Notes * n) : cursor(n->size()), notes(n) {}

void NotesNavigator::reset() {
    cursor = size();
}

void NotesNavigator::reset_end() {
    cursor = 0;
}

int NotesNavigator::current() {
    return cursor;
}

bool NotesNavigator::can_append() {
    return (cursor == size());
}

bool NotesNavigator::can_update() {
    return (0 <= cursor) < int(size());
}

int NotesNavigator::size() {
    return notes->size();
}

void NotesNavigator::next() {
    if (cursor < size())
        ++cursor;
    if (cursor == size())
        cursor = size() - 1;
}

void NotesNavigator::previous() {
    if (cursor > 0)
        --cursor;
}

std::string NotesNavigator::to_string() {
    std::ostringstream strs;
    std::string date, text;
    strs << "Content:" << std::endl;
    if (current() < 0) {
        strs << ">" << std::endl;
    }

    for (int i = 0; i < notes->size(); ++i) {
        strs << ((current() == i) ? "> " : "  ");
        strs << notes->get_notes()[i].to_string() << std::endl;
    }

    if (current() > size()) {
        strs << ">" << std::endl;
    }
    return strs.str();
}

}  // namespace QuickNotes
