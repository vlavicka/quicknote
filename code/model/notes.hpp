#ifndef HEADER_QUICKNOTE_MODEL_NOTES
#define HEADER_QUICKNOTE_MODEL_NOTES

#include <string>
#include <vector>

/*
 *  TODO: rewrite to use QString
 *  TODO: rework concept of getting notes -> state machine should be out of
 *        note class
 */

namespace QuickNotes {

class Note;

class Notes {
private:
    std::string filename_content;
    std::string filename_backup;
    std::vector<Note> content;
    std::vector<Note> backup;

public:
    static const std::string DATE_FORMAT;

    Notes(std::string filename);
    Notes(std::string filename, std::string backup);

    int size();
    std::string to_string();

    std::vector<Note> get_notes();

    Note get_note(int index);
    void append_note(std::string text);
    void insert_note(int index, std::string text);
    void update_note(int index, std::string text);
    Note delete_note(int index);

    void reload();
    void save();

    std::vector<Note> dbg_get_backup_notes();

private:
    Note parse_line(std::string line);
    std::vector<Note> process_file_content(std::string filename);
    void save_to_file(std::vector<Note> notes, std::string filename);

    // not implemented yet
    void set_filename(std::string filename);
    std::string get_backup_filename();

};

} // namespace QuickNote

#endif
