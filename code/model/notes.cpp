#include <iostream>
#include <ctime>
#include <sstream>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <stdexcept>

#include "../utils.hpp"
#include "strutils.hpp"
#include "model/note.hpp"
#include "model/notes.hpp"

namespace QuickNotes {

/*
 * TODO: rewrite to use utf-8 - files and strings
 */

const std::string Notes::DATE_FORMAT = "%Y-%m-%d %H:%M:%S";

Notes::Notes(std::string filename) 
        : filename_content(filename)
        , filename_backup(filename + ".backup") {}

Notes::Notes(std::string filename, std::string backup) 
        : filename_content(filename)
        , filename_backup(backup) {}

int Notes::size() {
    return content.size();
}

std::vector<Note> Notes::get_notes() {
    return content;
}

Note Notes::get_note(int index) {
    return content.at(index);
}

std::vector<Note> Notes::dbg_get_backup_notes() {
    return backup;
}

std::string Notes::to_string() {
    std::ostringstream strs;
    std::string date, text;
    strs << "Content:" << std::endl;
    for (auto i = 0u; i < content.size(); ++i) {
        strs << content[i].to_string() << std::endl;
    }
    strs << "Filename: " << filename_content << " (" << filename_backup << ")"
         << std::endl;
    return strs.str();
}

void Notes::append_note(std::string text) {
    auto date = QuickNotes::encode_time(time(nullptr), DATE_FORMAT);
    content.push_back(Note(date, text));
}

void Notes::insert_note(int index, std::string text) {
    assert(index < int(content.size()));
    content[index].set_text(text);
}

void Notes::update_note(int index, std::string text) {
    auto old_note = content[index];
    content[index] = Note(old_note.date(), text);
}

Note Notes::delete_note(int index) {
    auto result = content.at(index);
    backup.push_back(result);
    content.erase(content.begin() + index);
    return result;
}

Note Notes::parse_line(std::string line) {
    auto current = std::find_if(line.begin(), line.end(),
                                [](char c) { return c == '\t'; });
    int position = current - line.begin();
    std::string date = line.substr(0, position);
    std::string text = line.substr(++position, line.size());
    return Note(date, text);
}

std::vector<Note> Notes::process_file_content(std::string filename) {
    std::vector<Note> result;
    std::ifstream myfile(filename, std::ios::binary | std::ios::in);
    //std::cout << "Requesting file " << filename << std::endl;
    if (myfile.is_open()) {
        std::string line;
        while (std::getline(myfile, line, '\n')) {
            //std::cout << line << std::endl;
            result.push_back(parse_line(line));
        }
    }
    return result;
}

void Notes::reload() {
    content = process_file_content(filename_content);
    backup = process_file_content(filename_backup);
}

void Notes::save_to_file(std::vector<Note> notes, std::string filename) {
    std::ofstream myfile(filename, std::ios::binary | std::ios::out);
    for (auto &x : notes) {
        myfile << x.date() << '\t' << x.text() << '\n';
    }
}

void Notes::save() {
    save_to_file(content, filename_content);
    save_to_file(backup, filename_backup);
}

// not implemented yet
void Notes::set_filename(std::string filename) {
    /*
    assert filename, "Quick note file must be specified before update!"
    self._filename = filename
    self.reload
    */
    throw std::logic_error("Not implemented error!");
}

std::string Notes::get_backup_filename() {
    throw std::logic_error("Not implemented error!");
    /*
    head, tail = os.path.splitext(self._filename)
    return head + '-doner + tail
    */
    // return std::string("");
}
}  // namespace QuickNotes
