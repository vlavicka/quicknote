#ifndef STRUTILS_H
#define STRUTILS_H

#include <sstream>
#include <string>

template <typename InputIterator, typename T, typename Delimiter>
inline
std::string join_to_string(InputIterator first, 
                           InputIterator last, 
                           const Delimiter& delimiter, 
                           T fnc) {
    if (first == last)
        return std::string("");
    std::ostringstream strs;
    strs << fnc(*first);
    ++first;
    while (first != last) {
        strs << delimiter << fnc(*first);
        ++first;
    }
    return strs.str();
}

template <class InputIterator, typename Delimiter>
inline
std::string join_to_string(InputIterator first, 
                           InputIterator last, 
                           const Delimiter& delimiter) {
    if (first == last)
        return std::string("");
    std::ostringstream strs;
    strs << *first;
    ++first;
    while (first != last) {
        strs << delimiter << *first;
        ++first;
    }
    return strs.str();
}

template <typename T, typename Delimiter>
inline
std::string join_to_string(const T& a, const Delimiter& delimiter) {
    return join_to_string(a.begin(), a.end(), delimiter);
}

template <typename T, typename Delimiter>
inline
std::string join_to_string(T& a, const Delimiter& delimiter) {
    return join_to_string(a.begin(), a.end(), delimiter);
}

template <typename Container, typename Delimiter, typename Action>
inline
std::string join_to_string(Container& a, const Delimiter& delimiter, Action fnc) {
    return join_to_string(a.begin(), a.end(), delimiter, fnc);
}

template <typename Container, typename Delimiter, typename Action>
inline
std::string join_to_string(const Container& a, const Delimiter& delimiter, Action fnc) {
    return join_to_string(a.begin(), a.end(), delimiter, fnc);
}


#endif
