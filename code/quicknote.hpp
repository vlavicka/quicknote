#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <array>

#include "controls/texteditbox.hpp"
#include "controls/noteslistwidget.hpp"

#include "model/notesnavigator.hpp"

//#include <QxtGlobalShortcut>

namespace Ui {
class QuickNoteWindow;
}

namespace QuickNotes {

class QuickNoteWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit QuickNoteWindow(QWidget *parent = nullptr, std::string filename = "");
    ~QuickNoteWindow();

    void centerToDesktop();

private slots:
    void on_textEdit_textChanged();
    void enterNote(QString note);
    void cancelNote();
    void navigateNotes(TextEditBox::Action direction);
    void deleteNote(int index);

private:
    void updateNoteText(const QString &text);

    Ui::QuickNoteWindow *ui;
    Notes notes;
    NotesNavigator navigator;
    //QxtGlobalShortcut shortcut;
};

}

#endif // MAINWINDOW_H
