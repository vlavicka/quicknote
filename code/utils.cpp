#include <iostream>
#include <regex>
#include <ctime>
#include <cstdlib>
#include <cstring>

#include <QRect>

#include "utils.hpp"

namespace QuickNotes {

template <typename T>
inline std::string pair2str(T &x) {
    std::ostringstream strs;
    strs << "{'" << x.first << ": " << x.second << "'}";
    return strs.str();
}

void dump_tm_structure(std::tm &tm) {
    std::cout << "Year:   " << tm.tm_year << std::endl;
    std::cout << "Month:  " << tm.tm_mon << std::endl;
    std::cout << "Day:    " << tm.tm_mday << std::endl;
    std::cout << "Hour:   " << tm.tm_hour << std::endl;
    std::cout << "Minute: " << tm.tm_min << std::endl;
    std::cout << "Second: " << tm.tm_sec << std::endl;
    std::cout << "TS:     " << tm.tm_isdst << std::endl;
    std::cout << "------------------" << std::endl;
}

time_t decode_time(const std::string &data) {
    std::regex decoder(
        R"(^(\d{4})-(\d{2})-(\d{2})[\t ]+(\d{2}):(\d{2}):(\d{2})$)",
        std::regex::ECMAScript);
    std::smatch matched;

    auto current_time = std::time(nullptr);
    std::tm mytime;
    std::memcpy(&mytime, std::localtime(&current_time), sizeof(std::tm));
    if (std::regex_match(data, matched, decoder)) {
        mytime.tm_year = std::atoi(matched.str(1).c_str()) - 1900;
        mytime.tm_mon = std::atoi(matched.str(2).c_str()) - 1;
        mytime.tm_mday = std::atoi(matched.str(3).c_str());
        mytime.tm_hour = std::atoi(matched.str(4).c_str());
        mytime.tm_min = std::atoi(matched.str(5).c_str());
        mytime.tm_sec = std::atoi(matched.str(6).c_str());
        mytime.tm_isdst = 1;
    } else {
        throw std::runtime_error(
            "Error while converting time. Invalid input format.");
    }
    return std::mktime(&mytime);
}

std::string encode_time(time_t t, const std::string &format) {
    char mbstr[100];
    auto mytime = std::localtime(&t);
    auto written = std::strftime(mbstr, sizeof(mbstr), format.c_str(), mytime);
    if (written == 0) throw std::runtime_error("Error while converting time");
    return std::string(mbstr);
}

std::string encode_time(time_t t) {
    return encode_time(t, "%Y-%m-%d %H:%M:%S");
}
}

std::ostream& operator<<(std::ostream& os, const QRect &rect) {
    std::ostringstream strs;
    strs << "QRect(" << rect.x() << ", " 
                     << rect.y() << ", "
                     << rect.width() << ", "
                     << rect.height() << ")";
    return os << strs.str();
}


