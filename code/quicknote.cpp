#include <iostream>

#include <QScrollBar>
#include <QDesktopWidget>

#include "model/note.hpp"
#include "model/notes.hpp"

#include "quicknote.hpp"
#include "ui_quicknote.h"


// TODO: dark color for selection
// TODO: react properly on main frame re-size -> resize VBoxLayout control

namespace QuickNotes {

QuickNoteWindow::QuickNoteWindow(QWidget *parent, std::string filename)
    : QMainWindow(parent)
    , ui(new Ui::QuickNoteWindow)
    , notes(filename)
    , navigator(&notes)
    {
    ui->setupUi(this);

    connect(ui->textEdit, SIGNAL(returnPressed(QString)), this, SLOT(enterNote(QString)));
    connect(ui->textEdit, SIGNAL(escapePressed()), this, SLOT(cancelNote()));
    connect(ui->textEdit, SIGNAL(navigateNotes(TextEditBox::Action)),
            this, SLOT(navigateNotes(TextEditBox::Action)));
    connect(ui->tableWidgetNotes, SIGNAL(deleteNote(int)), this, SLOT(deleteNote(int)));

    notes.reload();
    navigator.reset();
    ui->tableWidgetNotes->updateNotes(notes);

    ui->textEdit->clearNote();
    centerToDesktop();
}

void QuickNoteWindow::centerToDesktop() {
    // TODO: allow user to change which screen to center if multiple screens
    //       are used
    QDesktopWidget wid;
    auto srect = wid.screenGeometry(0);

    auto width = frameGeometry().width();
    auto height = frameGeometry().height();
    setGeometry((srect.width() / 2) - (width / 2),
                (srect.height() / 2) - (height / 2), width, height);
}

QuickNoteWindow::~QuickNoteWindow() { delete ui; }

void QuickNoteWindow::on_textEdit_textChanged() {
    // std::cout << "Text changed" << std::endl;
}

void QuickNoteWindow::enterNote(QString note) {
    auto text = note;
    if (text.isEmpty()) {
        // TODO: show message in task bar
        //       "You have nothing to submit. Write text of a note and then
        //       press RETURN."
    } else {
        text.remove(QChar('\n'));
        if (navigator.can_append()) {
            notes.append_note(text.toStdString());
        } else {
            notes.insert_note(navigator.current(), text.toStdString());
            ui->statusBar->showMessage("Note has been changed.", 5000);
        }
        navigator.reset();
        ui->tableWidgetNotes->updateNotes(notes);
        ui->textEdit->clearNote();
        notes.save();
        // XXX: enable when problem with global shortcuts is solved
        std::cout << "XXX: setWindowState(Qt::WindowMinimized);" << std::endl;
        // setWindowState(Qt::WindowMinimized);
    }
}

void QuickNoteWindow::cancelNote() {
    ui->textEdit->clearNote();
    navigator.reset();
    std::cout << "XXX: setWindowState(Qt::WindowMinimized);" << std::endl;
    // XXX: enable when problem with global shortcuts is solved
    // setWindowState(Qt::WindowMinimized);
}

void QuickNoteWindow::navigateNotes(TextEditBox::Action direction) {
    switch (direction) {
    case TextEditBox::Action_NavigateUp:
        navigator.previous();
        updateNoteText(notes.get_note(navigator.current()).text().c_str());
        break;
    case TextEditBox::Action_NavigateDown:
        navigator.next();
        updateNoteText(notes.get_note(navigator.current()).text().c_str());
        break;
    default:
        std::cout << "Unsupported direction" << std::endl;
        break;
    }
    ui->tableWidgetNotes->selectRow(navigator.current());
}

void QuickNoteWindow::deleteNote(int index)
{
    this->notes.delete_note(index);
    this->notes.save();
}

void QuickNoteWindow::updateNoteText(const QString &text) {
    ui->textEdit->setNote(text);
}

}
