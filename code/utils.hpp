#ifndef HEADER_QUICKNOTE_UTILS
#define HEADER_QUICKNOTE_UTILS

#include <ctime>
#include <sstream>

class QRect;

std::ostream& operator<<(std::ostream& os, const QRect &rect);

namespace QuickNotes {

template <typename T> inline std::string pair2str(T &x);
time_t decode_time(const std::string &);
std::string encode_time(time_t t, const std::string &);
std::string encode_time(time_t t);

}
#endif
