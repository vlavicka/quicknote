#include <iostream>
#include <ctime>

#include <QScrollBar>
#include <QKeyEvent>
#include <QApplication>
#include <QClipboard>

#include "utils.hpp"

#include "model/note.hpp"
#include "model/notes.hpp"

#include "noteslistwidget.hpp"

namespace QuickNotes {

const std::array<int, 3> NotesListWidget::column_widths{{150, -1, 50}};

int get_days(std::string &t) {
    auto diff = std::difftime(std::time(nullptr), QuickNotes::decode_time(t));
    return int(diff / 3600 / 24);
}

QColor GetColor(int days, bool dark = false) {
    if (days > 20)
        return dark ? QColor(0xDD, 0x22, 0x22) : QColor(0xEE, 0x44, 0x44);
    else if (days > 10)
        return dark ? QColor(0xDD, 0x88, 0x88) : QColor(0xEE, 0xAA, 0xAA);
    else if (days > 5)
        return dark ? QColor(0xDD, 0xDD, 0x88) : QColor(0xEE, 0xEE, 0xAA);
    return dark ? QColor(0x88, 0xDD, 0x88) : QColor(0xAA, 0xEE, 0xAA);
}

void NotesListWidget::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Delete) {
        auto row = currentIndex().row();
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(item(row, 1)->text());
        removeRow(row);
        emit deleteNote(row);
    }

    QTableWidget::keyReleaseEvent(event);
}

NotesListWidget::NotesListWidget(QWidget * parent) : QTableWidget(parent) {
    this->initialize();
}

void NotesListWidget::resizeEvent(QResizeEvent * event) {
    this->setNotesHeader();
}

void NotesListWidget::initialize() {
    for (int i = 0; i < this->rowCount(); ++i) {
        this->removeRow(0);
    }
    this->setColumnCount(3);
    this->setRowCount(0);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void NotesListWidget::setNotesHeader() {
    auto frame_width = this->size().width();

    auto toolbar_width = this->verticalScrollBar()->isVisible() 
        ? this->verticalScrollBar()->geometry().width()
        : 0;
    this->setColumnWidth(0, column_widths[0]);
    this->setColumnWidth(
        1, frame_width - column_widths[0] - column_widths[2] - toolbar_width);
    this->setColumnWidth(2, column_widths[2]);
}

void NotesListWidget::setNoteItem(int row, int col, std::string content,
                                  QColor &color) {
    auto item = new QTableWidgetItem(content.c_str());
    auto brush = QBrush(color);
    item->setBackground(brush);
    this->setItem(row, col, item);
}

void NotesListWidget::updateNotes(QuickNotes::Notes &notes) {
    // TODO: this may be little bit inefficient - try to update only non
    // existent records
    this->initialize();
    for (auto &a : notes.get_notes()) {
        auto row = this->rowCount();
        auto days = a.days();
        auto color = GetColor(days);

        this->setRowCount(row + 1);
        this->setRowHeight(row, 20);

        setNoteItem(row, 0, a.date(), color);
        setNoteItem(row, 1, a.text(), color);
        setNoteItem(row, 2, std::to_string(days), color);
    }
    this->setNotesHeader();
}

}
