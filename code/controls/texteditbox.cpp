#include <iostream>

#include <QKeyEvent>
#include <array>
#include <algorithm>

#include "texteditbox.hpp"

namespace QuickNotes {

template <class T, class V>
inline
bool one_of(const T& c, const V &value)
{
    return (c.end() != std::find(c.begin(), c.end(), value));
}

TextEditBox::TextEditBox(QWidget * parent) : QTextEdit(parent) {
}

void TextEditBox::keyPressEvent(QKeyEvent * e) { 
    std::array<int, 2> enterKeys = { { Qt::Key_Return, Qt::Key_Enter } };
    if (e->modifiers() == Qt::ControlModifier)
    {
        if (e->key() == Qt::Key_Up) {
            emit navigateNotes(TextEditBox::Action_NavigateUp);
        } else if (e->key() == Qt::Key_Down) {
            emit navigateNotes(TextEditBox::Action_NavigateDown);
        } else {
            QTextEdit::keyPressEvent(e);
        }
    } else {
        if (one_of(enterKeys, e->key())) {
            auto text = toPlainText();
            emit returnPressed(text);
        } else if (e->key() == Qt::Key_Escape) {
            emit escapePressed();
        } else {
            QTextEdit::keyPressEvent(e);
        }
    }

    e->accept();
}

void TextEditBox::clearNote(bool focus) {
    setPlainText(QString(""));
    setDefaultStyle();
    if (focus) setFocus();
}

void TextEditBox::setNote(const QString &text) {
    clearNote(false);
    setPlainText(text);
}

void TextEditBox::setDefaultStyle()
{
    setFontPointSize(12.0);
    setFontItalic(true);
}

}
