#ifndef CUSTOM_TEXTEDITBOX_H
#define CUSTOM_TEXTEDITBOX_H

#include <iostream>
#include <QTextEdit>

namespace QuickNotes {

class TextEditBox : public QTextEdit
{
    Q_OBJECT

public:
    enum Action {
        Action_NavigateUp,
        Action_NavigateDown,
    };

    TextEditBox(QWidget * parent = nullptr);

    void clearNote(bool focus = true);
    void setNote(const QString &text);
    void setDefaultStyle();

protected:
    void keyPressEvent(QKeyEvent * e);

signals:
    void returnPressed(QString note);
    void escapePressed();
    void navigateNotes(TextEditBox::Action);
};

}

#endif
