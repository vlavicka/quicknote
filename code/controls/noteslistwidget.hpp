#ifndef HEADER_NOTES_LIST_HELPER
#define HEADER_NOTES_LIST_HELPER

#include <QTableWidget>
#include <array>
#include "model/notes.hpp"

namespace QuickNotes {

class NotesListWidget : public QTableWidget
{
    Q_OBJECT

private:
    static const std::array<int, 3> column_widths;
public:
    NotesListWidget(QWidget * parent = nullptr);
    void initialize();
    void updateNotes(QuickNotes::Notes &notes);

signals:
    void deleteNote(int index);

protected:
    void virtual resizeEvent(QResizeEvent * event);
    void virtual keyReleaseEvent(QKeyEvent *event);

private:
    void setNotesHeader();
    void setNoteItem(int row, int col, std::string content, QColor &color);

};

}


#endif
