#include <QApplication>

#include "quicknote.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int retval = 0;
    if (argc == 2) {
        std::string filename(argv[1]);
        QuickNotes::QuickNoteWindow w(nullptr, filename);
        w.show();
        retval = a.exec();
    } else {
        std::cout << "You have to specify file with notes!" << std::endl;
        retval = 1;
    }
    return retval;
}
