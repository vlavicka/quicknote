#!/bin/bash

set -e

if [ -e build/cmake ]; then
    export PATH=/usr/lib/ccache:$PATH
else
    mkdir -p build/cmake
fi

pushd build/cmake
if [ "$1" == "release" ]; then
    MY_BUILD_TYPE=Release
else
    MY_BUILD_TYPE=Debug
fi

cmake -DCMAKE_BUILD_TYPE=${MY_BUILD_TYPE} ../..
if [ "$1" == "verbose" ]; then
    make VERBOSE=1
else
    make
fi

cp quicknote ..

popd


echo "-------------------------------------------------------------"
echo " Running regression tests"
echo "-------------------------------------------------------------"

pushd build/cmake/test/unittest > /dev/null
./quicknote_test_all
popd > /dev/null
