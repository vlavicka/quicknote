@echo off
setlocal enabledelayedexpansion

set QTDIR=c:\Qt\5.4\msvc2013
set PYTHON_EXECUTABLE=c:\Python27\python.exe
set PYTHON_INCLUDE=c:\Python27\include
set PYTHON_LIBRARY=c:\Python27\libs\python27.lib
set PYTHON_LIBRARIES=c:\Python27\libs
set PYTHON_INCLUDE_DIRS=c:\Python27\include
set PATH=c:\dev\opendev\cmake\bin;c:\dev\opendev\swigwin-3.0.5;%PATH%
set CMAKE_PREFIX_PATH=c:\Qt\5.4\msvc2013\lib\cmake\Qt5;%CMAKE_PREFIX_PATH%

set BUILD_FOLDER=%~dp0\build\cmake
if not exist %BUILD_FOLDER% mkdir %BUILD_FOLDER%

pushd %BUILD_FOLDER%
cmake ../..
popd

endlocal
