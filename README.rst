==========
Quick Note
==========

Introduction
============

Quick note is tool for helping in situation when work flow is interrupted by 
outer (work unrelated) impulse that can be sudden idea, colleague distraction, 
or something that it is not important for the moment but has to be dealt later.

The main motivation is to keep on track when working with Pomodoro technique 
where work flow (called Pomodoro) is divided between 25 minutes of uninterrupted 
work and 5 minutes of break.

Idea is to provide keyboard shortcut that can trigger simple edit dialog from 
any application a user is working on. The user takes note and continue in 
interrupted work flow. He/she can revises note later when work is done (like 
break at the end of Pomodoro) and move the most important notes into other tool 
for further processing.


User interface
==============

=========== ============================================
Key         Action
=========== ============================================
Alt+F12     show hidden QuickNote window
ENTER       submit note or change note
ESCAPE      hide QuickNote window
Ctrl+Up     select previous note for update
Ctrl+Down   select next note for update
Alt+F4      exit application
=========== ============================================

When previous note is selected than ENTER update note with new text.

.. note::
   On Linux, you have to set global shortcut for Alt+F12 to run application.


Notes list
----------

Notes can be deleted by selecting them in list. Note content is copied into 
clipboard so it can be pasted whenever user wants. In addition note is backed 
up into file with suffix '-done'.


Highlighting notes
------------------

Notes are highlighted with colors so the user can be alerted when note is on 
the stack longer than some predefined time.

There are four levels of alert:

#. CRITICAL (RED) - note was inserted more than 21 days ago
#. LOOKAT (LIGHTRED) - note was inserted 11-20 days ago
#. EASY (YELLOW) - note was inserted 6-10 days ago
#. NEW (GREEN) - note is fresh

