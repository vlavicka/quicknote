#include <iostream>

#include "catch/catch.hpp"

#include "../../code/model/note.hpp"
#include "../../code/model/notes.hpp"
#include "../../code/model/notesnavigator.hpp"
#include "../../code/utils.hpp"

class NotesTestsFixture {
protected:
    QuickNotes::Notes notes;
    QuickNotes::NotesNavigator navigator;
public:
    NotesTestsFixture() : notes("testfile"), navigator(&notes) {}
};

TEST_CASE_METHOD(NotesTestsFixture, "create_notes", "[model]")
{
    notes.append_note("test note 01");
    notes.append_note("test note 02");
    notes.append_note("test note 03");
    CHECK(notes.get_note(0).text() == "test note 01");
}

void create_notes(QuickNotes::Notes& notes) {
    notes.append_note("test note 01");
    notes.append_note("test note 02");
    notes.append_note("test note 03");
}

std::string get_resource(std::string name) {
    std::ostringstream sout;
    std::string thisdir(__FILE__);
    thisdir.erase(thisdir.begin() + thisdir.find_last_of('/'), thisdir.end());
    sout << thisdir << "/resources/" << name;
    return sout.str();
}

std::string get_result(std::string name) {
    std::ostringstream sout;
    std::string thisdir(__FILE__);
    thisdir.erase(thisdir.begin() + thisdir.find_last_of('/'), thisdir.end());
    sout << thisdir << "/results/" << name;
    return sout.str();
}


TEST_CASE("test_notes_operations", "[model]")
{
    QuickNotes::Notes notes("testfile");
    QuickNotes::NotesNavigator navigator(&notes);

    auto CHECK_CURRENT = [&] (std::string expected) {
        CHECK(notes.get_note(navigator.current()).text() == expected);
    };

    SECTION("insert_note")
    {
        notes.append_note("test note 01");
        CHECK("test note 01" == notes.get_note(0).text());
    }

    SECTION("delete_note_01")
    {
        create_notes(notes);
        auto note_text = notes.delete_note(1).text();
        CHECK(notes.size() == 2);
        CHECK(note_text == "test note 02");
    }

    SECTION("delete_note_02")
    {
        create_notes(notes);
        auto note_text = notes.delete_note(2).text();
        CHECK(notes.size() == 2);
        CHECK(note_text == "test note 03");
    }

    SECTION("delete_note_03")
    {
        create_notes(notes);
        auto note_text = notes.delete_note(0).text();
        CHECK(notes.size() == 2);
        CHECK(note_text == "test note 01");
    }

    SECTION("delete_note_04")
    {
        create_notes(notes);
        CHECK_THROWS_AS(notes.delete_note(3), std::out_of_range);
        CHECK(notes.size() == 3);
    }

    SECTION("backup_of_deleted_notes")
    {
        create_notes(notes);
        notes.delete_note(0);
        auto backup_notes = notes.dbg_get_backup_notes();
        CHECK(backup_notes.size() == 1);
        CHECK(backup_notes[0].text() == "test note 01");
    }

    SECTION("update_note")
    {
        notes.append_note("test note 01");
        notes.append_note("test note 02");

        navigator.reset();
        navigator.previous();
        CHECK_CURRENT("test note 02");
        navigator.previous();
        notes.update_note(navigator.current(), "test note 01 - changed");
        CHECK_CURRENT("test note 01 - changed");
        CHECK(notes.get_note(0).text() == "test note 01 - changed");

        navigator.next();
        notes.update_note(navigator.current(), "test note 02 - changed");
        CHECK_CURRENT("test note 02 - changed");
        CHECK(notes.get_note(1).text() == "test note 02 - changed");
    }

    SECTION("navigate_through_notes")
    {
        for (int i = 1; i < 6; ++i) {
            std::ostringstream sout;
            sout << "test note " << i;
            notes.append_note(sout.str());
        }

        navigator.reset();
        navigator.next();
        CHECK_CURRENT("test note 5");
        CHECK(notes.get_note(0).text() == "test note 1");
        
        navigator.next();
        CHECK_CURRENT("test note 5");

        navigator.previous();
        navigator.previous();
        navigator.previous();
        CHECK_CURRENT("test note 2");

        navigator.previous();
        navigator.previous();
        navigator.previous();
        CHECK(notes.get_note(0).text() == "test note 1");
        navigator.next();
        navigator.next();
        navigator.next();
        navigator.next();
        navigator.next();
        navigator.next();
        CHECK_CURRENT("test note 5");
    }

}

TEST_CASE("test_update_content", "[model]")
{
    auto filename = get_resource("notes_test.txt");
    auto notes = QuickNotes::Notes(filename);
    notes.reload();
    CHECK(notes.get_note(0).text() == "test note 01");
    CHECK(notes.get_note(1).text() == "test note 02");
    CHECK(notes.get_note(2).text() == "test note 03");
}

TEST_CASE("test_save_content", "[model]") {
    auto filename = get_result("test_save_content-g.txt");
    auto notes = QuickNotes::Notes(filename);
    notes.append_note("test note 01");
    notes.append_note("test note 02");
    notes.append_note("test note 03");
    notes.save();
    notes = QuickNotes::Notes(filename);
    notes.reload();
    CHECK(notes.get_note(0).text() == "test note 01");
    CHECK(notes.get_note(1).text() == "test note 02");
    CHECK(notes.get_note(2).text() == "test note 03");
}

TEST_CASE("test_save_content_and_backup", "[model]")
{
    auto filename = get_result("test_save_content_and_backup-g.txt");
    auto filename_backup = get_result("test_save_content_and_backup-backup-g.txt");
    auto notes = QuickNotes::Notes(filename, filename_backup);
    notes.append_note("test note 01");
    notes.append_note("test note 02");
    notes.append_note("test note 03");
    notes.delete_note(1);
    notes.save();

    notes = QuickNotes::Notes(filename, filename_backup);
    notes.reload();
    CHECK(notes.get_note(0).text() == "test note 01");
    CHECK(notes.get_note(1).text() == "test note 03");
    
    auto backup_notes = notes.dbg_get_backup_notes();
    CHECK(backup_notes.size() == 1);
    CHECK(backup_notes[0].text() == "test note 02");
}


TEST_CASE("test_data_decoder", "[utils]")
{
    std::string date("2014-08-16 09:41:32");
    auto mytime = QuickNotes::decode_time(date);
    CHECK(QuickNotes::encode_time(mytime, "%Y-%m-%d %H:%M:%S") == date);
    CHECK(QuickNotes::encode_time(mytime) == date);
}

TEST_CASE("test_data_decoder_error", "[utils]")
{
    CHECK_THROWS_AS(QuickNotes::decode_time("14-08-16 09:41:32"), std::runtime_error);
}



