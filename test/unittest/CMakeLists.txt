# test with CTest definition
include_directories(${Boost_INCLUDE_DIRS} . ../../code ../external)

set(SOURCES_TEST_ALL
    test_all.cpp
    quicknote_notes.cpp
)

add_executable(quicknote_test_all ${SOURCES_TEST_ALL})
target_link_libraries(quicknote_test_all model_lib rt pthread ${Boost_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

