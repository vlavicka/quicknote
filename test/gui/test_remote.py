import os
import sys
import time
import unittest

__thisdir__ = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(__thisdir__, ".."))
import remote

remote.SOCKET_TIMEOUT = 1

def callback():
    print "ACTIVATE"

class TestRemoteControl(unittest.TestCase):
    def test_run_server(self):
        server = remote.RemoteControlServer()
        server.register_callback("ACTIVATE", callback)
        server.start()
        time.sleep(0.1)

        client = remote.RemoteControlClient()
        client.send_activate()
        del client

        time.sleep(0.5)
        server.stop()
        server.join()
