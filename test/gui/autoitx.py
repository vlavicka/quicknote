"""AutoItXControl wrapper for working wit AutoIt COM

======================
GUI Automation library
======================

Introduction
============

AutoItX module is a wrapper of AutoItCom control whis is distributed with AutoIt 
installation. Main goal is to create adapter for AutoIt functions and pythonize it.

Module also contains extended functionality of AutoIt tool. Main features of this 
extension are:

    - create stack of windows (push and pop operations)
    - send text with specified key delay and wait time after typing
    - easier control of GUI Controls
    - easier manipulation with windows


Module configuration
====================

When using extension of library, it is possible control speed of command execution with environmental 
variable ``PYTHONIT_REPLAY_SPEED`` (if not set, default *"speed"* value is *1.0*). First of all, 
it involves *sleep* method and send methods that primarily use delays for assuring that system has
time to finish particular operation.

.. todo::
    Use another environmental varibale to set key delays speed independently. It can be
    ``PYTHOINT_SEND_KEY_SPEED`` or something in this sense.



AutoItX wrapper - *class* AutoItXControl
========================================

Class is a simple wrapper of COM calls to AutoItXCtrl.

AutoItX options
---------------

.. include:: autoitx_options.inc



AutoItX extension - *class* AutoItXControlExt
=============================================

<notes>


"""
import win32com.client
import logging
import os

log = logging.getLogger("AutoItXControl")

def get_wait_time(orig_time):
    return int(orig_time * get_replay_speed())

def get_replay_speed():
    def fnc():
        return float(result)
    result = os.environ.get('PYTHONIT_REPLAY_SPEED', "1.0")
    log.debug('Replay speed ratio: %s', result)
    return fnc

get_replay_speed = get_replay_speed()

class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls.instance = None
    def __call__(cls, *args, **kw):
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.instance

class AutoItError(BaseException): pass

_control_options = [
        "CaretCoordMode",
        "MouseClickDelay",
        "MouseClickDownDelay",
        "MouseClickDragDelay",
        "MouseCoordMode",
        "PixelCoordMode",
        "SendAttachMode",
        "SendCapslockMode",
        "SendKeyDelay",
        "SendKeyDownDelay",
        "WinDetectHiddenText",
        "WinSearchChildren",
        "WinTextMatchMode",
        "WinTitleMatchMode",
        "WinWaitDelay",
        ]

class AutoItXControl(object):
    """
    >>> a = AutoItXControl()
    >>> a.CaretCoordMode
    1
    >>> a.MouseClickDelay
    10
    >>> a.MouseClickDownDelay
    10
    >>> a.MouseClickDragDelay 
    250
    >>> a.MouseCoordMode 
    0
    >>> a.PixelCoordMode 
    1
    >>> a.SendAttachMode 
    0
    >>> a.SendCapslockMode
    1
    >>> a.SendKeyDelay 
    1
    >>> a.SendKeyDownDelay
    1
    >>> a.WinDetectHiddenText
    0
    >>> a.WinSearchChildren
    0
    >>> a.WinTextMatchMode
    1
    >>> a.WinTitleMatchMode
    4
    >>> a.WinWaitDelay
    100
    """
    __metaclass__ = Singleton
    def __init__(self, wintitle=""):
        self._wintitle = wintitle
        self.wintext = ""
        self.autoit = win32com.client.Dispatch("AutoItX3.Control")
        # set default values for options
        self.CaretCoordMode = 1
        self.MouseClickDelay = 10
        self.MouseClickDownDelay = 10
        self.MouseClickDragDelay  = 250
        self.MouseCoordMode = 0
        self.PixelCoordMode = 1
        self.SendAttachMode = 0
        self.SendCapslockMode = 1
        self.SendKeyDelay = 1
        self.SendKeyDownDelay = 1
        self.WinDetectHiddenText = 0
        self.WinSearchChildren = 0
        self.WinTextMatchMode = 1
        self.WinTitleMatchMode = 2
        self.WinWaitDelay = 100

    def __setattr__(self, name, value):
        """Set attribute - used only for options
        >>> autoit = AutoItXControl()
        >>> autoit.WinWaitDelay = 50
        >>> print autoit.WinWaitDelay
        50
        >>> autoit.WinTitleMatchMode = 2
        >>> print autoit.WinTitleMatchMode
        2
        """
        if name in _control_options:
            if name in ['SendKeyDownDelay', 'SendKeyDelay']:
                value = get_wait_time(int(value))
            self.opt(name, value)
        else:
            object.__setattr__(self, name, value)

    @property
    def wintitle(self):
        return self._wintitle
    @wintitle.setter
    def wintitle(self, title):
        assert self.win_exists(title), "Window with title '%s' does not exist!" % title
        self._wintitle = title

    def error(self, ): self.autoit.errro()
    def version(self, ): self.autoit.version()
    # ENVIRONMENT MANAGEMENT FUNCTIONS
    def clip_get(self, ): return self.autoit.ClipGet()
    def clip_put(self, *args): return self.autoit.ClipPut(*args)
    # FILE, DIRECTORY AND MANAGEMENT
    def drive_map_add(self, *args): return self.autoit.DriveMapAdd(*args)
    def drive_map_del(self, *args): return self.autoit.DriveMapDel(*args)
    def drive_map_get(self, *args): return self.autoit.DriveMapGet(*args)
    def ini_delete(self, *args): return self.autoit.IniDelete(*args)
    def ini_read(self, *args): return self.autoit.IniRead(*args)
    def ini_write(self, *args): return self.autoit.IniWrite(*args)
    # GRAPHIC FUNCTIONS
    def pixel_checksum(self, *args): return self.autoit.PixelChecksum(*args)
    def pixel_get_color(self, *args): return self.autoit.PixelGetColor(*args)
    def pixel_search(self, *args): return self.autoit.PixelSearch(*args)
    # KEYBOARD CONTROL
    def send(self, *args): self.autoit.Send(*args)
    # MESSAGE BOXES AND DIALOGS
    def tool_tip(self, *args): self.autoit.ToolTip(*args)
    # MISC. FUNCTIONS
    def autoit_set_option(self, *args): 
        """
        >>> a = AutoItXControl()
        >>> a.opt('WinTitleMatchMode', 2)
        2
        >>> a.opt('WinTitleMatchMode')
        2
        """
        name = args[0]
        try:
            value = args[1]
        except IndexError:
            value = None
        if value is None:
            return self.__dict__[args[0]]
        else:
            old = self.autoit.Opt(*args)
            self.__dict__[args[0]] = args[1]
            return old

    def opt(self, *args): return self.autoit_set_option(*args)
    def block_input(self, *args): self.autoit.BlockInput(*args)
    def cd_tray(self, *args): return self.autoit.CDTray(*args)
    def url_download_to_file(self, *args): return self.autoit.URLDownloadToFile(*args)
    def is_admin(self, ): return self.autoit.IsAdmin()
    # MOUSE CONTROL
    def mouse_click(self, *args): self.autoit.MouseClick(*args)
    def mouse_click_drag(self, *args): self.autoit.MouseClickDrag(*args)
    def mouse_down(self, *args): self.autoit.MouseDown(*args)
    def mouse_get_cursor(self, *args): return self.autoit.MouseGetCursor(*args)
    def mouse_get_pos_x(self, *args): return self.autoit.MouseGetPosX(*args)
    def mouse_get_pos_y(self, *args): return self.autoit.MouseGetPosY(*args)
    def mouse_move(self, *args): return self.autoit.MouseMove(*args)
    def mouse_up(self, *args): self.autoit.MouseUp(*args)
    def mouse_wheel(self, *args): self.autoit.MouseWheel(*args)
    # PROCESS MANAGEMENT
    def process_close(self, *args): self.autoit.ProcessClose(*args)
    def process_exists(self, *args): return self.autoit.ProcessExists(*args)
    def process_set_priority(self, *args): return self.autoit.ProcessSetPriority(*args)
    def process_wait(self, *args): return self.autoit.ProcessWait(*args)
    def process_wait_close(self, *args): return self.autoit.ProcessWaitClose(*args)
    def run(self, *args): return self.autoit.Run(*args)
    def run_as_set(self, *args): return self.autoit.RunAsSet(*args)
    def run_wait(self, *args): return self.autoit.RunWait(*args)
    def shutdown(self, *args): return self.autoit.Shutdown(*args)
    # REGISTRY MANAGEMENT
    def reg_delete_key(self, *args): return self.autoit.RegDeleteKey(*args)
    def reg_delete_val(self, *args): return self.autoit.RegDeleteVal(*args)
    def reg_enum_key(self, *args): return self.autoit.RegEnumKey(*args)
    def reg_enum_val(self, *args): return self.autoit.RegEnumVal(*args)
    def reg_read(self, *args): return self.autoit.RegRead(*args)
    def reg_write(self, *args): return self.autoit.RegWrite(*args)
    # TIMER AND DELAY FUNCTIONS
    def sleep(self, wtime): self.autoit.Sleep(get_wait_time(wtime))
    # WINDOW MANAGEMENT
    def control_click(self, *args): return self.autoit.ControlClick(*args)
    def control_command(self, *args): return self.autoit.ControlCommand(*args)
    def control_disable(self, *args): return self.autoit.ControlDisable(*args)
    def control_enable(self, *args): return self.autoit.ControlEnable(*args)
    def control_focus(self, *args): return self.autoit.ControlFocus(*args)
    def control_get_focus(self, *args): return self.autoit.ControlGetFocus(*args)
    #def control_get_handle(self, *args): return self.autoit.ControlGetHandle(*args)
    def control_get_handle(self, *args): return int(self.autoit.ControlGetHandle(*args), 16)

    def control_get_pos_height(self, *args): return int(self.autoit.ControlGetPosHeight(*args))
    def control_get_pos_width(self, *args): return int(self.autoit.ControlGetPosWidth(*args))
    def control_get_pos_x(self, *args): return int(self.autoit.ControlGetPosX(*args))
    def control_get_pos_y(self, *args): return int(self.autoit.ControlGetPosY(*args))
    def control_get_text(self, *args): return self.autoit.ControlGetText(*args)
    def control_hide(self, *args): return self.autoit.ControlHide(*args)
    def control_list_view(self, *args): return self.autoit.ControlListView(*args)
    def control_move(self, *args): return self.autoit.ControlMove(*args)
    def control_send(self, *args): return self.autoit.ControlSend(*args)
    def control_set_text(self, *args): return self.autoit.ControlSetText(*args)
    def control_show(self, *args): return self.autoit.ControlShow(*args)
    def control_tree_view(self, *args): return self.autoit.ControlTreeView(*args)
    def statusbar_get_text(self, *args): return self.autoit.StatusbarGetText(*args)
    def win_activate(self, *args): self.autoit.WinActivate(*args)
    def win_active(self, *args): return bool(int(self.autoit.WinActive(*args)))
    def win_close(self, *args): self.autoit.WinClose(*args)
    def win_exists(self, *args): return bool(int(self.autoit.WinExists(*args)))
    def win_get_caret_pos_x(self, *args): return int(self.autoit.WinGetCaretPosX(*args))
    def win_get_caret_pos_y(self, *args): return int(self.autoit.WinGetCaretPosY(*args))
    def win_get_class_list(self, *args): return self.autoit.WinGetClassList(*args)
    def win_get_client_size_height(self, *args): return self.autoit.WinGetClientSizeHeight(*args)
    def win_get_client_size_width(self, *args): return self.autoit.WinGetClientSizeWidth(*args)
    def win_get_handle(self, *args): return int(self.autoit.WinGetHandle(*args), 16)
    def win_get_pos_height(self, *args): return self.autoit.WinGetPosHeight(*args)
    def win_get_pos_width(self, *args): return self.autoit.WinGetPosWidth(*args)
    def win_get_pos_x(self, *args): return self.autoit.WinGetPosX(*args)
    def win_get_pos_y(self, *args): return self.autoit.WinGetPosY(*args)
    def win_get_process(self, *args): return self.autoit.WinGetProcess(*args)
    def win_get_state(self, *args): return self.autoit.WinGetState(*args)
    def win_get_text(self, *args): return self.autoit.WinGetText(*args)
    def win_get_title(self, *args): return self.autoit.WinGetTitle(*args)
    def win_kill(self, *args): self.autoit.WinKill(*args)
    def win_menu_select_item(self, *args): return self.autoit.WinMenuSelectItem(*args)
    def win_minimize_all(self, *args): self.autoit.WinMinimizeAll(*args)
    def win_minimizel_all_undo(self, *args): self.autoit.WinMinimizeAllUndo(*args)
    def win_move(self, *args): self.autoit.WinMove(*args)
    def win_set_on_top(self, *args): self.autoit.WinSetOnTop(*args)
    def win_set_state(self, *args): self.autoit.WinSetState(*args)
    def win_set_title(self, *args): self.autoit.WinSetTitle(*args)
    def win_set_trans(self, *args): return self.WinSetTrans(*args)
    def win_wait(self, *args): return self.autoit.WinWait(*args)
    def win_wait_active(self, *args): return self.autoit.WinWaitActive(*args)
    def win_wait_close(self, *args): return bool(int(self.autoit.WinWaitClose(*args)))
    def win_wait_not_active(self, *args): return self.autoit.WinWaitNotActive(*args)


class GControl(object):
    def __init__(self, id):
        self._id = id
        self._name = ''

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name


class GCheckBoxControl(GControl):
    def __init__(self, id, checked = False):
        super(GCheckBoxControl, self).__init__(id)
        self._checked = checked

    def reset(self):
        self.uncheck()

    def is_checked(self):
        return self._checked

    def check(self):
        if not self._checked:
            log.debug("Checking control '%s'" % self._id)
            #autoit.control_command(self._id, "Check", "")
            autoit.control_send(self._id, "{SPACE}")
            self._checked = True
        autoit.sleep(50)

    def uncheck(self):
        if self._checked:
            log.debug("Unchecking control '%s'" % self._id)
            #autoit.control_command(self._id, "Check", "")
            autoit.control_send(self._id, "{SPACE}")
            self._checked = False
        autoit.sleep(50)


class GComboBoxControl(GControl):
    def __init__(self, id, values):
        super(GComboBoxControl, self).__init__(id)
        self._values = values
        self.reset()

    def reset(self):
        self._value = self._values[0]
        autoit.control_send(self._id, "{HOME}")
        autoit.sleep(50)

    def select(self, text):
        assert text in self._values
        log.debug("Selecting text '%s' in [%s]" % (text, self._id))
        autoit.control_send(self._id, "{HOME}")
        autoit.sleep(50)
        for name in self._values:
            if name == text.lower():
                self._value = name
                return
            autoit.control_send(self._id, "{DOWN}")
            autoit.sleep(10)
        autoit.sleep(50)

    def get_selection(self):
        return self._value



class AutoItXControlExt(AutoItXControl):
    """Extended version of original AutoIt control.
    """
    __metaclass__ = Singleton
    def __init__(self, wintitle=""):
        AutoItXControl.__init__(self, wintitle)
        self.apps = {}
        self._window_stack = []

    def __str__(self):
        return "AutoItXControl"

    def __getattribute__(self, name):
        return super(AutoItXControlExt, self).__getattribute__(name)

    def __setattr__(self, name, value):
        super(AutoItXControlExt, self).__setattr__(name, value)

    # extension methods
    def win_wait_for(self, title = "", text = "", timeout = 5):
        """Check whether window exist and wait for its activation"""
        if not title: title = self._wintitle
        log.debug("Check for existing windows '%s' with text '%s'" % (title, text))
        if not self.win_wait(title, text, timeout):
            msg = "Window with tile '%s' and text " "'%s' does not exist!" % (title, text)
            log.debug(msg)
            raise AutoItError(msg)
        log.debug("Window was found! Waiting for its activation.")
        if not self.win_active(title, text):
            self.win_activate(title, text)
        self.win_wait_active(title, text)
        self._wintitle = title

    def send(self, text, wait=0, raw=False):
        """ Send text to window """
        log.debug("Sending text: %s" % text)
        super(AutoItXControlExt, self).send(text, raw)
        self.sleep(wait)
    
    def xsend(self, text, delay=1, wait=0, raw=False):
        """ Send text to window with temporary set key delay """
        log.debug("Sending text: %s" % text)
        old_key_delay = self.autoit.Opt("SendKeyDelay", get_wait_time(delay))
        super(AutoItXControlExt, self).send(text, raw)
        self.opt("SendKeyDelay", old_key_delay)
        self.sleep(wait)

    def msg_box(self, title, text):
        """ Show message box with title and textual information """
        import tkMessageBox
        tkMessageBox.askokcancel(title, text)

    def push_window(self, wintitle=""):
        """ Push window to stack """
        title = self.win_get_title(wintitle)
        log.info("Title form window: %s" % title)
        self._window_stack.append(title)
        log.info("Title[%i]: %s" % (len(self._window_stack), self._window_stack[-1]))

    def pop_window(self):
        """ Pop windows from stack """
        if self._window_stack:
            title = self._window_stack.pop()
            self.win_wait_for(title, "", 5)
            log.info("Window with title '%s' was removed from window stack." % title)
        else:
            log.info("There is no window to pop!")

    def control_focus(self, title="", text="", ctrlid=""):
        """ Focus specified control """
        # TODO: remove title and text dependency
        if not title:
            title = self._wintitle
        return super(AutoItXControlExt, self).control_focus(title, text, ctrlid)

    def control_get_focus(self):
        return super(AutoItXControlExt, self).control_get_focus(self._wintitle, self.wintext)

    def win_get_text(self, title="", text=""):
        """ Get text of particular windows. """
        # TODO: remove title and text dependency
        return super(AutoItXControlExt, self).win_get_text(title, text)

    def control_command(self, ctrlid, command, value):
        """ Send command to control. """
        return super(AutoItXControlExt, self).control_command(self._wintitle, self.wintext, ctrlid, command, value)

    def control_send(self, ctrlid, text):
        """ Send text to control """
        return super(AutoItXControlExt, self).control_send(self._wintitle, self.wintext, ctrlid, text)

    #def win_get_text(self):
    #    return super(AutoItXControlExt, self).win_get_text(self._wintitle, self.wintext)

    def control_get_size(self, ctrlid):
        """ Get size of the control bounding box. """
        width = super(AutoItXControlExt, self).control_get_pos_width(self._wintitle, self.wintext, ctrlid)
        height = super(AutoItXControlExt, self).control_get_pos_height(self._wintitle, self.wintext, ctrlid)
        return (width, height)

    def control_get_pos(self, ctrlid):
        """ Get position of the control """
        x = super(AutoItXControlExt, self).control_get_pos_x(self._wintitle, self.wintext, ctrlid)
        y = super(AutoItXControlExt, self).control_get_pos_y(self._wintitle, self.wintext, ctrlid)
        return (x, y)

    def control_get_text(self, ctrlid):
        return super(AutoItXControlExt, self).control_get_text(self._wintitle, self.wintext, ctrlid)

    def control_click(self, ctrlid, button='left', clicks=1, x=1, y=1):
        return super(AutoItXControlExt, self).control_click(self._wintitle, self.wintext, ctrlid, button, clicks, x, y)

    def win_get_pos(self):
        x = super(AutoItXControlExt, self).win_get_pos_x(self._wintitle, self.wintext)
        y = super(AutoItXControlExt, self).win_get_pos_y(self._wintitle, self.wintext)
        return (x, y)

    def win_get_size(self):
        width = super(AutoItXControlExt, self).win_get_pos_width(self._wintitle, self.wintext)
        height = super(AutoItXControlExt, self).win_get_pos_height(self._wintitle, self.wintext)
        return (width, height)

    def win_get_client_size(self):
        width = super(AutoItXControlExt, self).win_get_client_size_width(self._wintitle, self.wintext)
        height = super(AutoItXControlExt, self).win_get_client_size_height(self._wintitle, self.wintext)
        return (width, height)

    def win_move(self, x, y, width=0, height=0):
        args = [self._wintitle, self.wintext, x, y]
        if width: args.append(width)
        if height: args.append(height)
        super(AutoItXControlExt, self).win_move(*args)

    def mouse_get_pos(self):
        pos_x = self.mouse_get_pos_x()
        pos_y = self.mouse_get_pos_y()
        return pos_x, pos_y

    def control_tree_view(self, ctrlid, command, opt1='', opt2=''):
        return super(AutoItXControlExt, self).control_tree_view(self._wintitle, '', ctrlid, command, opt1, opt2)


autoit = AutoItXControlExt()

if __name__ == "__main__":
    import doctest
    doctest.testmod()
