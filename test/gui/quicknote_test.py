import unittest
import os
import sys
import time

THISDIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(THISDIR, '..'))

import quicknote

try:
    import ldtp, ooldtp
except ImportError:
    pass

import subprocess
class Automation(object):
    def __init__(self):
        self._context = None

    def run(self, application):
        pid = subprocess.Popen(application).pid

    def win_wait_for(self, wintitle=None):
        if not self._context:
            self._context = ooldtp.context(wintitle)
            ldtp.activatewindow(wintitle)
        else:
            self._context.activatewindow()
            
        self.sleep(500)

    def send(self, text, wait=100):
        if text == "!{F4}":
            self._context.closewindow()
        else:
            text = text.replace('{', '<').replace('}', '>')
            ldtp.generatekeyevent(text)
        self.sleep(100)

    def sleep(self, msecs):
        import time
        time.sleep(float(msecs) / 1000)

    def control_list_view(self, title, text, ctrlid, cmd, opt1, opt2):
        if cmd == 'GetText':
            pass
        elif cmd == 'Select':
            #self._context.selectindex(ctrlid, opt1)
            pass
        

if sys.platform == "win32":
    from autoitx import autoit
    print "running on windows - using AutoIt"
    application = 'c:\Python27\pythonw.exe quicknote_coverage.py'
else:
    print "running on Linux - using ldtp"
    autoit = Automation()
    application = ['python', 'quicknote_coverage.py']


WINTITLE = 'Quick Note'

class TestQuickNotesGUI(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestQuickNotesGUI, cls).setUpClass()
        cls.ctrlid_list = 'SysListView321'
        autoit.win_wait_for(WINTITLE)

    def control_view(self, command, option1='', option2=''):
        return autoit.control_list_view(WINTITLE, '', self.ctrlid_list, 
                                        command, option1, option2)

    def test_01_write_note(self):
        note_text = 'AUTOIT: note'
        autoit.win_wait_for()
        autoit.send('%s{ENTER}' % note_text, wait=100)

        print "DEBUG:", autoit._context.getobjectlist()

        self.control_view('Select', 0)
        self.assertEqual(note_text, self.control_view('GetText', 0, 1))

    @unittest.skip('')
    def test_02_delete_note(self):
        note_text = 'AUTOIT: note to be deleted'
        autoit.send('%s{ENTER}' % note_text, wait=100)
        self.control_view('Select', 1)
        autoit.control_send(self.ctrlid_list, '{DELETE}')
        autoit.sleep(50)
        self.assertEqual(note_text, autoit.clip_get())

    @unittest.skip('')
    def test_03_write_note_and_hide(self):
        note_text = 'AUTOIT: hidden note'
        autoit.send('%s{ENTER}' % note_text, wait=100)
        autoit.sleep(200)
        self.assertEqual(note_text, self.control_view('GetText', 1, 1))

    @unittest.skip('')
    def test_04_cancel_note_editing(self):
        note_text = 'AUTOIT: hidden note'
        autoit.send('%s{ESC}' % note_text, wait=100)
        autoit.sleep(200)
        autoit.send('!{F12}', wait=200)
        self.assertEqual(2, int(self.control_view('GetItemCount')))

    @unittest.skip('')
    def move_to_note(self, movement):
        autoit.xsend('^{%s}^a^c' % movement, delay=10, wait=100)
        return autoit.clip_get()

    @unittest.skip('')
    def test_05_select_previous_note(self):
        autoit.send('AUTOIT: temporary note')
        result = self.move_to_note('UP')
        self.assertEqual('AUTOIT: hidden note', result)
        result = self.move_to_note('DOWN')
        self.assertEqual('AUTOIT: temporary note', result)

    @unittest.skip('')
    def test_06_insert_empty_note(self):
        autoit.send('{ENTER}')
        self.assertEqual(2, int(self.control_view('GetItemCount')))

    @unittest.skip('')
    def test_07_change_note(self):
        autoit.send('^{UP}^a')
        note_text = 'AUTOIT: changed note'
        autoit.send('%s{ENTER}' % note_text, wait=100)
        self.control_view('Select', 0)
        self.assertEqual(note_text, self.control_view('GetText', 1, 1))

    @unittest.skip('')
    def test_08_select_first_note(self):
        for index in range(3):
            result = self.move_to_note('UP')
        self.assertEqual('AUTOIT: note', result)

    @unittest.skip('')
    def test_09_select_last(self):
        for index in range(3):
            result = self.move_to_note('UP')
        result = self.move_to_note('DOWN')
        self.assertEqual('AUTOIT: changed note', result)

def setUpModule():
    if os.path.isfile('_quicknote.txt'):
        os.remove('_quicknote.txt')
    autoit.run(application)

def tearDownModule():
    autoit.win_wait_for(WINTITLE)
    autoit.sleep(1000)
    autoit.send('!{F4}')
