import unittest
import os
import sys
import time

import coverage

cov = coverage.coverage()
cov.start()

THISDIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(THISDIR, '..'))

import notes

class TestQuickNotes(unittest.TestCase):
    def setUp(self):
        self.note = notes.QuickNotes(None)

    def test_insert_note(self):
        self.note.insert_note('test note 01')
        self.assertEqual('test note 01', self.note._content[0][1])

    def test_update_note(self):
        self.note.insert_note('test note 01')
        self.note.insert_note('test note 02')
        self.assertEqual('test note 02', self.note.previous())
        self.note.previous()
        self.note.insert_note('test note 01 - changed')
        self.assertEqual('test note 01 - changed', self.note._content[0][1])

    def create_notes(self):
        self.note.insert_note('test note 01')
        self.note.insert_note('test note 02')
        self.note.insert_note('test note 03')

    def test_delete_note_01(self):
        self.create_notes()
        date_, note_text = self.note.delete_note(1)
        self.assertEqual(2, len(self.note))
        self.assertEqual('test note 02', note_text)

    def test_delete_note_02(self):
        self.create_notes()
        date_, note_text = self.note.delete_note(2)
        self.assertEqual(2, len(self.note))
        self.assertEqual('test note 03', note_text)

    def test_delete_note_03(self):
        self.create_notes()
        date_, note_text = self.note.delete_note(0)
        self.assertEqual(2, len(self.note))
        self.assertEqual('test note 01', note_text)

    def test_delete_note_04(self):
        self.create_notes()
        self.assertRaises(IndexError, self.note.delete_note, 3)
        self.assertEqual(3, len(self.note))


    # additional tests
    def test_update_content(self):
        filename = __file__.replace('.py', '.txt')
        note = notes.QuickNotes(None)
        note.set_filename(filename)
        self.assertEqual('test note 01', note._content[0][1])
        self.assertEqual('test note 02', note._content[1][1])
        self.assertEqual('test note 03', note._content[2][1])

    def test_save_content(self):
        filename = __file__.replace('.py', '-01.txt')
        note = notes.QuickNotes(filename)
        note.insert_note('test note 01')
        self.assertEqual('test note 01', note._content[0][1])
        note.insert_note('test note 02')
        self.assertEqual('test note 02', note._content[1][1])
        note.insert_note('test note 03')
        self.assertEqual('test note 03', note._content[2][1])
        note.save()

        note = notes.QuickNotes(filename)
        note.update_content()
        self.assertEqual('test note 01', note._content[0][1])
        self.assertEqual('test note 02', note._content[1][1])
        self.assertEqual('test note 03', note._content[2][1])




def tearDownModule():
    cov.stop()
    cov.save()
    cov.html_report()
