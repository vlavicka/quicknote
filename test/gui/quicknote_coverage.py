import sys
import os
import coverage


cov = coverage.coverage()
cov.start()

THISDIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(THISDIR, '..'))
import quicknote

quicknote.main(dict(output=os.path.join(THISDIR, '_quicknote.txt')))

cov.stop()
cov.save()
cov.html_report()
